<?php
	session_start();
	include "koneksi.php";
	
	if(!isset($_SESSION['akses'])){
		header('Location:login.php');
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dashboard::PT. Karya Pak Oles Tokcer</title>

    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Sistem Informasi Logistik PT. Bokashi</a>
            </div>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="index.php"><i class="fa fa-home fa-fw"></i> Home</a>
                        </li>
						<?php if($_SESSION['akses'] == 'Cabang'){ ?>
                        <li>
                            <a href="#"><i class="fa fa-database fa-fw"></i> Master Data<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="masterKantor.php" class="active">Master Kantor</a></li>
                                <li><a href="masterKaryawan.php">Master Karyawan</a></li>
                                <li><a href="masterBarang.php">Master Barang</a></li>
                                <li><a href="masterHarga.php">Master Harga</a></li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
						<?php } ?>
						<?php if($_SESSION['akses'] == 'Unit' || $_SESSION['akses'] == 'Cabang'){ ?>
                        <li>
                            <a href="#"><i class="fa fa-tasks fa-fw"></i> Transaksi<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="trOrder.php">Order Barang</a></li>
                                <li><a href="trSetor.php">Setor Barang</a></li>
                                <li><a href="trRetur.php">Retur Barang</a></li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
						<?php } ?>
                        <li>
                            <a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12"></div>
            </div>

            <div class="row">
				<?php if($_SESSION['akses'] == 'Cabang'){ ?>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                            <h3>Menu Master Data</h3>
                        <div class="panel-heading">
                        </div>

                        <div class="panel-body">
							<p><a href="masterKantor.php"><button type="button" class="btn btn-outline btn-primary btn-lg">Master Kantor</button></a> &nbsp;
							<a href="masterKaryawan.php"><button type="button" class="btn btn-outline btn-primary btn-lg">Master Karyawan</button></a>&nbsp;
							<a href="masterBarang.php"><button type="button" class="btn btn-outline btn-primary btn-lg">Master Barang</button></a>&nbsp;
							<a href="masterHarga.php"><button type="button" class="btn btn-outline btn-primary btn-lg">Master Harga</button></a>&nbsp;</p>
                        </div>
                    </div>
                </div>
				<?php } ?>
				
				<?php if($_SESSION['akses'] == 'Unit' || $_SESSION['akses'] == 'Cabang'){ ?>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3>Menu Transaksi</h3>
                        </div>

                        <div class="panel-body">
							<p><a href="trOrder.php"><button type="button" class="btn btn-outline btn-primary btn-lg">Order Barang</button></a>&nbsp;
							<a href="trSetor.php"><button type="button" class="btn btn-outline btn-primary btn-lg">Setor Barang</button></a>&nbsp;
							<a href="trRetur.php"><button type="button" class="btn btn-outline btn-primary btn-lg">Retur Barang</button></a></p>
                        </div>
                    </div>
                </div>
				<?php } ?>
            </div>
        </div>
    </div>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <script src="dist/js/sb-admin-2.js"></script>
</body>
</html>