<?php
	session_start();
	include "koneksi.php";
	$qry = "SELECT * FROM setoran";
	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
?>

<?php include "header.php"; ?>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Transaksi Setor</title>
</head>

<body>
	<div id="page-wrapper">
		<div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Setoran</h1>
				<p style="text-align: right"><a href="formSetor.php"><button type="button" class="btn btn-success">Tambah Data</button></a></p>
            </div>
        </div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-hover" id="dataTables">
								<thead>
									<tr>
										<th>No Setor</th>
										<th>Tanggal Setor</th>
										<th>Total Setor</th>
										<th>Keterangan</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php
									$i=1;
									while($isi = mysqli_fetch_array($sql)){
										echo "
											<tr>
												<td>$isi[id_setoran]</td>
												<td>$isi[tgl_setoran]</td>
												<td>". number_format($isi['total_harga_setoran']) . "</td>
												<td>$isi[keterangan_setoran]</td>
												<td>
													<a href='lihatSetor.php?id=$isi[id_setoran]'>Lihat Data</a>";
													if($_SESSION['akses'] == 'Unit') { echo " | <a href='ubahSetor.php?id=$isi[id_setoran]'>Ubah Data</a>"; }
										echo "
												</td>
											</tr>
										";
										$i++;
									}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<?php include "footer.php"; ?>
</html>