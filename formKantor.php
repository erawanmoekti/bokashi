<?php
	session_start();
	include "koneksi.php";
	$kode = "";
	$nama = "";
	$alamat = "";
	$telp = "";
	
	if(isset($_GET['id'])){
		$kode = $_GET['id'];
		$kondisi = "Ubah";
		
		$qry = "SELECT * FROM kantor'";
		$sql = mysqli_query($con, $qry);
		$isi = mysqli_fetch_array($sql);

		$nama = $isi['nama_kantor'];
		$alamat = $isi['alamat_kantor'];
		$telp = $isi['telp_kantor'];
	}else{
		$kondisi = "Tambah";
	}
	
	if(isset($_POST['kode_kantor'])){
		$kode = $_POST['kode_kantor'];
		$nama = $_POST['nama_kantor'];
		$alamat = $_POST['alamat_kantor'];
		$telp = $_POST['telp_kantor'];
		$kondisi = $_POST['kondisi'];

		if($kondisi == "Tambah"){
			$qry = "INSERT INTO kantor (id_kantor, nama_kantor, alamat_kantor, telp_kantor)
						 VALUES ('$kode', '$nama', '$alamat', '$telp')";
			mysqli_query($con, $qry) or die(mysqli_error($con));

			header('Location: masterKantor.php?ket=sukses_simpan');
		}else if($kondisi == "Ubah"){
		}
	}
?>

<?php include "header.php"; ?>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Form Kantor</title>
</head>

<body>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Form Tambah Data Kantor</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
									<form role="form" action="" method="post">
                                        <div class="form-group">
                                            <label>Kode Kantor</label>
                                            <input class="form-control" type="text" name="kode_kantor" />
                                        </div>

                                        <div class="form-group">
                                            <label>Nama Kantor</label>
                                            <input class="form-control" type="text" name="nama_kantor" />
                                        </div>

                                        <div class="form-group">
                                            <label>Alamat Kantor</label>
                                            <input class="form-control" type="text" name="alamat_kantor" />
                                        </div>

                                        <div class="form-group">
                                            <label>Telp Kantor</label>
                                            <input class="form-control" type="text" name="telp_kantor" />
                                        </div>

										<input type="hidden" name="kondisi" value="<?php echo $kondisi; ?>" />
										<center><button type="submit" name="btnTambah" class="btn btn-default" />Simpan</button></center>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</body>
<?php include "footer.php"; ?>
</html>