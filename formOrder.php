<?php
	session_start();
	include "koneksi.php";

	$id_kantor = $_SESSION['kantor'];
	$id_karyawan = $_SESSION['id_karyawan'];
	$qry = "SELECT * FROM kantor WHERE id_kantor = '$id_kantor'";
	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
	$isi = mysqli_fetch_array($sql);
	$nama_kantor = $isi["nama_kantor"];
	
	$no_order = "";
	$tgl_order = date('Y-m-d');
	$ket_order = "";
	
	$qry = "SELECT id_pesan FROM pesan WHERE MONTH(tgl_pesan) = MONTH(CURDATE()) AND YEAR(tgl_pesan) = YEAR(CURDATE()) ORDER BY id_pesan DESC";
	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
	$isi = mysqli_fetch_array($sql);
	$tmp = $isi['id_pesan'];
	
	if($tmp == ""){
		$no_order = "ORD-" .date('y') . date('m') . "-0001";
	}else{
		$tmp = substr($tmp, 9, 4) + 1;
		$no_order = "ORD-" .date('y') . date('m') . "-" . str_pad($tmp, 4, "0", STR_PAD_LEFT);
	}
	
	$kondisi = "Tambah";
	//echo $tmp;
	
	if(isset($_POST['btnSimpan'])){
		$no_order = $_POST['no_order'];
		$tgl_order = $_POST['tgl_order'];
		$ket_order = $_POST['ket_order'];
		$kondisi = $_POST['kondisi'];

		if($kondisi == "Tambah"){
			//Simpan Detail Pesan
			$idx = 0; $total = 0;
			
			foreach($_POST['id_barang'] as $id_barang){
				$id_harga = $_POST['id_harga'][$idx];
				$qry = "SELECT * FROM harga WHERE id_harga = '$id_harga'";
				$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
				$isi = mysqli_fetch_array($sql);
				$harga = $isi['harga_barang'];
				
				$qty = $_POST['qty'][$idx];
				$total += $qty * $harga;
					
				$qry = "INSERT INTO detail_pesan (id_pesan, id_barang, id_harga, jumlah_barang_pesan, harga_satuan_pesan)
							 VALUES ('$no_order', '$id_barang', '$id_harga', $qty, $harga)";
				$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
					
				$idx++;
			}
			
			//Simpan Pesan
			$qry = "INSERT INTO pesan (id_pesan, tgl_pesan, total_harga_pesan, keterangan_pesan, id_kantor)
						 VALUES ('$no_order', '$tgl_order', '$total', '$ket_order', '$id_kantor')";
			mysqli_query($con, $qry) or die(mysqli_error($con));

			header('Location: trOrder.php?ket=sukses_simpan');
		}else if($kondisi == "Ubah"){
		}
	}
?>

<?php include "header.php"; ?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Order Barang</title>
</head>

<body>
	<div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Form Order Barang</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
									<form role="form" action="" method="post">
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>No Order</label>
													<input class="form-control" type="text" id="no_order" name="no_order" value="<?php echo $no_order; ?>" readonly />
												</div>
												
												<div class="form-group">
													<label>Kantor Unit</label>
													<input class="form-control" type="text" name="nama_kantor" value="<?php echo $nama_kantor; ?>" readonly>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label>Tanggal Order</label>
													<input class="form-control" type="date" name="tgl_order" value="<?php echo $tgl_order; ?>" />
												</div>
												
												<div class="form-group">
													<label>Keterangan</label>
													<input class="form-control" type="text" name="ket_order" value="<?php echo $ket_order; ?>" />
												</div>
											</div>
										</div>
										
										<input type="hidden" name="kondisi" value="<?php echo $kondisi; ?>" />
										
										<div class="row">
											<div class="col-lg-12">
												<div class="panel panel-default">
													<div class="panel-body">
														<div class="dataTable_wrapper">
															<table class="table table-striped table-bordered table-hover" id="dataTables">
																<thead>
																	<tr>
																		<th>Nama Barang</th>
																		<th>Harga</th>
																		<th>Qty</th>
																		<th>Nilai</th>
																	</tr>
																</thead>
																<tbody>
																<?php
																	$idx = 0;
																	$qry = "SELECT barang.*, harga.harga_barang FROM barang INNER JOIN harga ON barang.id_harga = harga.id_harga ORDER BY barang.nama_barang";
																	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
																	while($isi = mysqli_fetch_array($sql)){ ?>
																		<tr height='30px'>
																			<td>
																				<?php echo $isi["nama_barang"]; ?>
																				<input type="hidden" name="id_barang[<?php echo $idx; ?>]" value="<?php echo $isi["id_barang"]; ?>">
																				<input type="hidden" name="id_harga[<?php echo $idx; ?>]" value="<?php echo $isi["id_harga"]; ?>">
																			</td>
																			<td><input type="text" id="harga<?php echo $idx; ?>" name="harga[<?php echo $idx; ?>]" value="<?php echo $isi["harga_barang"]; ?>" readonly onblur="hitung(<?php echo $idx; ?>)"></td>
																			<td><input type="text" id="qty<?php echo $idx; ?>" name="qty[<?php echo $idx; ?>]" value="0" onblur="hitung(<?php echo $idx; ?>)"></td>
																			<td><input type="text" id="total<?php echo $idx; ?>" name="total[<?php echo $idx; ?>]" value="0" readonly></td>
																		</tr>
																		<?php $idx++;
																	}
																?>
																</tbody>
																<input type="hidden" name="kondisi" value="<?php echo $kondisi; ?>" />
																<input type="hidden" name="jml_tl" value="<?php echo $jml_tl; ?>" />
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
										<p style="text-align: right"><input type="submit" name="btnSimpan" value="Simpan Order" class="btn btn-success"/></p>
									</form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</body>
<?php include "footer.php"; ?>
<script>
	function hitung(idx){
		var harga, qty, total;
		harga = document.getElementById("harga"+idx).value;
		qty = document.getElementById("qty"+idx).value;
		total = parseInt(harga) * parseInt(qty);
		document.getElementById("total"+idx).value = total;
	}
</script
</html>