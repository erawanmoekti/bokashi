<?php
	session_start();
	include "koneksi.php";
	$qry = "SELECT barang.*, harga.harga_barang
			  FROM barang INNER JOIN harga
			    ON barang.id_harga = harga.id_harga";
	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
?>

<?php include "header.php"; ?>

	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Master Barang</h1>
				<p style="text-align: right"><a href="formBarang.php"><button type="button" class="btn btn-success">Tambah Data</button></a></p>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-hover" id="dataTables">
								<thead>
									<tr>
										<th>ID Barang</th>
										<th>Nama Barang</th>
										<th>Harga Barang</th>
										<th>Kadaluarsa</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php
									while($isi = mysqli_fetch_array($sql)){
										echo "
											<tr>
												<td>$isi[id_barang]</td>
												<td>$isi[nama_barang]</td>
												<td>$isi[harga_barang]</td>
												<td>$isi[kadaluarsa]</td>
												<td><a href='hapusBarang.php?id=$isi[id_barang]'><button type='button' class='btn btn-danger btn-sm'><i class='fa fa-trash'></button></a></td>
											</tr>
										";
									}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php include "footer.php"; ?>