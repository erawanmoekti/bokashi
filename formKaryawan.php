<?php
	session_start();
	include "koneksi.php";
	$kode = "";
	$nama = "";
	$alamat = "";
	$status = "";
	$telp = "";
	$kantor = "";
	$username = "";
	$password = "";
	
	if(isset($_GET['id'])){
		$kode = $_GET['id'];
		$kondisi = "Ubah";
		
		$qry = "SELECT karyawan.*, kantor.id_karyawan
				  FROM karyawan INNER JOIN kantor
					ON karyawan.id_karyawan = kantor.id_karyawan
				 WHERE karyawan.id_karyawan = '$kode'";
		$sql = mysqli_query($con, $qry);
		$isi = mysqli_fetch_array($sql);

		$nama = $isi['nama_karyawan'];
		$alamat = $isi['alamat_karyawan'];
		$status = $isi['status_karyawan'];
		$telp = $isi['telp_karyawan'];
		$id_karyawan = $isi['id_karyawan'];
		$kantor = $isi['nama_karyawan'];
		$username = $isi['username'];
		$password = $isi['password'];
	}else{
		$kondisi = "Tambah";
	}
	
	if(isset($_POST['kode_karyawan'])){
		$kode = $_POST['kode_karyawan'];
		$nama = $_POST['nama_karyawan'];
		$alamat = $_POST['alamat_karyawan'];
		$status = $_POST['status_karyawan'];
		$telp = $_POST['telp_karyawan'];
		$id_karyawan = $_POST['kantor'];
		$username = $_POST['username'];
		$password = $_POST['password'];
		$kondisi = $_POST['kondisi'];

		if($kondisi == "Tambah"){
			$qry = "INSERT INTO karyawan (id_karyawan, nama_karyawan, alamat_karyawan, status_karyawan, telp_karyawan, id_karyawan, username, password)
						 VALUES ('$kode', '$nama', '$alamat', '$status', '$telp', '$id_karyawan', '$username', '$password')";
			mysqli_query($con, $qry) or die(mysqli_error($con));
			
			header('Location: masterKaryawan.php?ket=sukses_simpan');
		}else if($kondisi == "Ubah"){
		}
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Form Karyawan</title>

    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>
	<div id="wrapper">
		<?php include "header.php"; ?>
		<div id="page-wrapper">
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Form Tambah Data Karyawan</h1>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
									<form role="form" action="" method="post">
                                        <div class="form-group">
                                            <label>Kode Karyawan</label>
                                            <input class="form-control" type="text" name="kode_karyawan" />
                                        </div>

                                        <div class="form-group">
                                            <label>Nama Karyawan</label>
                                            <input class="form-control" type="text" name="nama_karyawan" />
                                        </div>

                                        <div class="form-group">
                                            <label>Jabatan Karyawan</label>
                                            <input class="form-control" type="text" name="jabatan_karyawan" />
                                        </div>

                                        <div class="form-group">
                                            <label>Status Karyawan</label>
											<select class="form-control">
												<option value="Cabang">Cabang</option>
												<option value="Unit">Unit</option>
												<option value="TL">Team Leader</option>
											</select>
                                        </div>
										<div class="form-group">
                                            <label>Kantor</label>
                                            <select class="form-control">
											<?php
												$qry2 = "SELECT * FROM kantor";
												$sql2 = mysqli_query($con, $qry2) or die(mysqli_error($con));
												while($isi2 = mysqli_fetch_array($sql2)){
													echo "<option value='$isi2[id_kantor]'>$isi2[nama_kantor]</option>";
												}
											?>
											  </select>
                                        </div>
										<div class="form-group">
                                            <label>Username</label>
                                            <input class="form-control" type="text" name="username" />
                                        </div><div class="form-group">
                                            <label>Password</label>
                                            <input class="form-control" type="password" name="p" />
                                        </div>
										<input type="hidden" name="kondisi" value="<?php echo $kondisi; ?>" />
										<button type="submit" name="btnTambah" class="btn btn-default" />Simpan</button></center>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
	<?php include "footer.php"; ?>
</body>
</html>