<?php
	session_start();
	include "koneksi.php";

	$no_setoran = $_GET['id'];
	$id_kantor = $_SESSION['kantor'];
	$qry = "SELECT setoran.*, kantor.nama_kantor
			  FROM setoran INNER JOIN kantor
			    ON setoran.id_kantor = kantor.id_kantor
			 WHERE setoran.id_setoran = '$no_setoran'";
	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
	$isi = mysqli_fetch_array($sql);
	
	function get_qty_tl($id_tl, $id_barang){
		global $con, $no_setoran;
		$qry = "SELECT * FROM detail_setoran WHERE id_setoran = '$no_setoran' AND id_barang = '$id_barang' AND id_karyawan = '$id_tl'";
		$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
		$isi = mysqli_fetch_array($sql);
		if(mysqli_num_rows($sql) == 0){
			return 0;
		}else{
			return $isi['jumlah_barang_setoran'];
		}
	}
?>

<?php include "header.php"; ?>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Transaksi Setoran</title>
</head>

<body>
	<div id="page-wrapper">
		<div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">FAKTUR SETORAN</h1>
            </div>
        </div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>No Setoran</label>
									<label>: <?php echo $no_setoran; ?></label>
								</div>
								
								<div class="form-group">
									<label>Kantor Unit</label>
									<label>: <?php echo $isi["nama_kantor"]; ?></label>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Tanggal Setoran</label>
									<label>: <?php echo $isi["tgl_setoran"]; ?></label>
								</div>
								
								<div class="form-group">
									<label>Keterangan</label>
									<label>: <?php echo $isi["keterangan_setoran"]; ?></label>
								</div>
							</div>
						</div>

						<div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-hover" id="dataTables">
								<tbody>
								<?php
									echo "<tr height='30px'><td><b>Nama Barang</b></td><td>Harga</td>";
									$jml_tl = 0; $tl = Array();
									$qry = "SELECT * FROM karyawan WHERE id_kantor = '$id_kantor' AND status_karyawan = 'TL'";
									$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
									while($isi = mysqli_fetch_array($sql)){
										echo "<td>" . $isi['nama_karyawan'] . "</td>";
										$jml_tl++; $tl[] = $isi['id_karyawan'];
									}
									echo "<td>Nilai</td></tr>";
									
									$qry = "SELECT detail_setoran.*, barang.nama_barang
											  FROM detail_setoran INNER JOIN barang 
											    ON detail_setoran.id_barang = barang.id_barang
											 GROUP BY detail_setoran.id_barang
											 ORDER BY barang.nama_barang";
									$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
									while($isi = mysqli_fetch_array($sql)){
										$id_barang = $isi['id_barang'];
										$harga = $isi['harga_satuan_setoran'];
										echo "<tr height='30px'><td>$isi[nama_barang]</td><td>$isi[harga_satuan_setoran]</td>";

										$qry2 = "SELECT * FROM karyawan WHERE id_kantor = '$id_kantor' AND status_karyawan = 'TL'";
										$sql2 = mysqli_query($con, $qry2) or die(mysqli_error($con));

										$jml = 0;
										while($isi2 = mysqli_fetch_array($sql2)){
											$qty_tl = get_qty_tl($isi2['id_karyawan'], $id_barang);
											echo "<td>" . $qty_tl . "</td>";
											$jml += $qty_tl;
										}
										echo "<td>". number_format($jml * $harga) ."</td></tr>";
									}
								?>
								</tbody>
								<input type="hidden" name="kondisi" value="<?php echo $kondisi; ?>" />
								<input type="hidden" name="jml_tl" value="<?php echo $jml_tl; ?>" />
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<?php include "footer.php"; ?>
</html>