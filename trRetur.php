<?php
	session_start();
	include "koneksi.php";
	$qry = "SELECT * FROM retur";
	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
?>
	
<?php include "header.php"; ?>
	
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Transaksi Retur</title>
</head>

<body>
	<div id="page-wrapper">
		<div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Retur Barang</h1>
				<p style="text-align: right"><a href="formRetur.php"><button type="button" class="btn btn-success">Tambah Data</button></a></p>
            </div>
        </div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-hover" id="dataTables">
								<thead>
									<tr>
										<th>No Retur</th>
										<th>Tanggal Retur</th>
										<th>Total Retur</th>
										<th>Keterangan</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php
									$i=1;
									while($isi = mysqli_fetch_array($sql)){
										echo "
											<tr>
												<td>$isi[id_retur]</td>
												<td>$isi[tgl_retur]</td>
												<td>". number_format($isi['total_harga_retur']) . "</td>
												<td>$isi[keterangan_retur]</td>
												<td>
													<a href='lihatRetur.php?id=$isi[id_retur]'>Lihat Data</a>";
													if($_SESSION['akses'] == 'Unit') { echo " | <a href='ubahRetur.php?id=$isi[id_retur]'>Ubah Data</a>"; }
										echo "
												</td>
											</tr>
										";
										$i++;
									}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<?php include "footer.php"; ?>
</html>