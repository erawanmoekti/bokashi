<?php
	session_start();
	include "koneksi.php";
	$kode = "";
	$nama = "";
	$satuan = "";
	$kadaluarsa = "";
	$jml = "";
	$harga = "";

	if(isset($_GET['id'])){
		$kode = $_GET['id'];
		$kondisi = "Ubah";
		
		$qry = "SELECT barang.*, harga.harga_barang
				  FROM barang INNER JOIN harga
					ON barang.id_harga = harga.id_harga
				 WHERE barang.id_barang = '$kode'";
		$sql = mysqli_query($con, $qry);
		$isi = mysqli_fetch_array($sql);

		$nama = $isi['nama_barang'];
		$satuan = $isi['isi_satuan'];
		$kadaluarsa = $isi['kadaluarsa'];
		$jml = $isi['jumlah_persediaan'];
		$id_harga = $isi['id_harga'];
		$harga = $isi['harga_barang'];
	}else{
		$kondisi = "Tambah";
	}
	
	if(isset($_POST['kode_barang'])){
		$kode = $_POST['kode_barang'];
		$nama = $_POST['nama_barang'];
		$satuan = $_POST['isi_satuan'];
		$kadaluarsa = $_POST['kadaluarsa'];
		$jml = $_POST['jml_persediaan'];
		$id_harga = "HRG" . $_POST['kode_barang'];
		$harga = $_POST['harga'];
		$kondisi = $_POST['kondisi'];

		if($kondisi == "Tambah"){
			$qry = "INSERT INTO barang (id_barang, nama_barang, isi_satuan, kadaluarsa, jumlah_persediaan, id_harga)
						 VALUES ('$kode', '$nama', '$satuan', '$kadaluarsa', '$jml', '$id_harga')";
			mysqli_query($con, $qry) or die(mysqli_error($con));
			
			$qry = "INSERT INTO harga (id_harga, harga_barang)
						 VALUES ('$id_harga', '$harga')";
			mysqli_query($con, $qry) or die(mysqli_error($con));

			header('Location: masterBarang.php?ket=sukses_simpan');
		}else if($kondisi == "Ubah"){
		}
	}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Form Barang</title>

    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>
	<div id="wrapper">
        <?php include "header.php"; ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Form Tambah Data Barang</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
									<form role="form" action="" method="post">
                                        <div class="form-group">
                                            <label>Kode Barang</label>
                                            <input class="form-control" type="text" name="kode_barang" />
                                        </div>

                                        <div class="form-group">
                                            <label>Nama Barang</label>
                                            <input class="form-control" type="text" name="nama_barang" />
                                        </div>

                                        <div class="form-group">
                                            <label>Isi Satuan</label>
                                            <input class="form-control" type="text" name="isi_satuan" />
                                        </div>

                                        <div class="form-group">
                                            <label>Kadaluarsa</label>
                                            <input class="form-control" type="text" name="kadaluarsa" />
                                        </div>
										<div class="form-group">
                                            <label>Jumlah Persediaan</label>
                                            <input class="form-control" type="text" name="jml_persediaan" />
                                        </div>
										<div class="form-group">
                                            <label>Harga</label>
                                            <input class="form-control" type="text" name="harga" />
                                        </div>

										<input type="hidden" name="kondisi" value="<?php echo $kondisi; ?>" />
										<center><button type="submit" name="btnTambah" class="btn btn-default" />Simpan</button></center>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
    <?php include "footer.php"; ?>
</body>
</html>