<?php
	session_start();
	include "koneksi.php";

	$id_kantor = $_SESSION['kantor'];
	$id_karyawan = $_SESSION['id_karyawan'];
	$qry = "SELECT * FROM kantor WHERE id_kantor = '$id_kantor'";
	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
	$isi = mysqli_fetch_array($sql);
	$nama_kantor = $isi["nama_kantor"];
	
	$no_setoran = "";
	$tgl_setoran = date('Y-m-d');
	$ket_setoran = "";
	
	function get_qty_tl($id_tl, $id_barang){
		global $con, $no_setoran;
		$qry = "SELECT * FROM detail_setoran WHERE id_setoran = '$no_setoran' AND id_barang = '$id_barang' AND id_karyawan = '$id_tl'";
		$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
		$isi = mysqli_fetch_array($sql);
		if(mysqli_num_rows($sql) == 0){
			return 0;
		}else{
			return $isi['jumlah_barang_setoran'];
		}
	}

	if(isset($_GET['id'])){
		$no_setoran = $_GET['id'];
		$kondisi = "Ubah";
		
		$qry = "SELECT * FROM setoran WHERE id_setoran = '$no_setoran'";
		$sql = mysqli_query($con, $qry);
		$isi = mysqli_fetch_array($sql);

		$tgl_setoran = $isi['tgl_setoran'];
		$ket_setoran = $isi['keterangan_setoran'];
	}
	//echo $tmp;
	
	if(isset($_POST['btnSimpan'])){
		$no_setoran = $_POST['no_setoran'];
		$tgl_setoran = $_POST['tgl_setoran'];
		$ket_setoran = $_POST['ket_setoran'];
		$kondisi = $_POST['kondisi'];

		//Simpan Detail Pesan
		$qry = "DELETE FROM detail_setoran WHERE id_setoran = '$no_setoran'";
		$sql = mysqli_query($con, $qry) or die(mysqli_error($con));

		$idx = 0; $total = 0;
		$jml_tl = $_POST['jml_tl'];
		
		foreach($_POST['id_barang'] as $id_barang){
			$id_harga = $_POST['id_harga'][$idx];
			$qry = "SELECT * FROM harga WHERE id_harga = '$id_harga'";
			$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
			$isi = mysqli_fetch_array($sql);
			$harga = $isi['harga_barang'];
			
			for($i=1; $i<=$jml_tl; $i++){
				$tl = $_POST['id_tl'.$i][$idx];
				$qty = $_POST['qty_tl'.$i][$idx];
				$total += $qty * $harga;
				
				$qry = "INSERT INTO detail_setoran (id_setoran, id_barang, id_harga, id_karyawan, jumlah_barang_setoran, harga_satuan_setoran)
							 VALUES ('$no_setoran', '$id_barang', '$id_harga', '$tl', $qty, $harga)";
				$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
				
				//echo "Barang : $id_barang, Harga : $harga, Qty TL" . $i . " : $qty";
			}
			$idx++;
		}
		
		//Simpan Pesan
		$qry = "UPDATE setoran
				   SET tgl_setoran = '$tgl_setoran',
					   total_harga_setoran = '$total',
					   keterangan_setoran = '$ket_setoran'
				WHERE id_setoran = '$no_setoran'";
		mysqli_query($con, $qry) or die(mysqli_error($con));

		header('Location: trSetor.php?ket=sukses_simpan');
	}
?>

<?php include "header.php"; ?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Setoran</title>
</head>

<body>
	<div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Form Setoran</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
									<form role="form" action="" method="post">
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>No Setor</label>
													<input class="form-control" type="text" id="no_setoran" name="no_setoran" value="<?php echo $no_setoran; ?>" readonly />
												</div>
												
												<div class="form-group">
													<label>Kantor Unit</label>
													<input class="form-control" type="text" name="nama_kantor" value="<?php echo $nama_kantor; ?>" readonly>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label>Tanggal Setor</label>
													<input class="form-control" type="date" name="tgl_setoran" value="<?php echo $tgl_setoran; ?>" />
												</div>
												
												<div class="form-group">
													<label>Keterangan</label>
													<input class="form-control" type="text" name="ket_setoran" value="<?php echo $ket_setoran; ?>" />
												</div>
											</div>
										</div>
										
										<input type="hidden" name="kondisi" value="<?php echo $kondisi; ?>" />
										
										<div class="row">
											<div class="col-lg-12">
												<div class="panel panel-default">
													<div class="panel-body">
														<div class="dataTable_wrapper">
															<table class="table table-striped table-bordered table-hover" id="dataTables">
																<tbody>
																<?php
																	echo "<tr height='30px'><td><b>Nama Barang</b></td><td>Harga</td>";
																	$jml_tl = 0; $tl = Array();
																	$qry = "SELECT * FROM karyawan WHERE id_kantor = '$id_kantor' AND status_karyawan = 'TL'";
																	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
																	while($isi = mysqli_fetch_array($sql)){
																		echo "<td>" . $isi['nama_karyawan'] . "</td>";
																		$jml_tl++; $tl[] = $isi['id_karyawan'];
																	}
																	echo "<td>Nilai</td></tr>";
																	
																	$idx = 0; $jml = 0;
																	$qry = "SELECT barang.*, harga.harga_barang FROM barang INNER JOIN harga ON barang.id_harga = harga.id_harga ORDER BY barang.nama_barang";
																	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
																	while($isi = mysqli_fetch_array($sql)){
																		echo "<tr height='30px'><td>
																			$isi[nama_barang]
																			<input type='hidden' name='id_barang[$idx]' value='$isi[id_barang]'>
																			<input type='hidden' name='id_harga[$idx]' value='$isi[id_harga]'>
																		</td><td><input type='text' id='harga$idx' name='harga[$idx]' value='$isi[harga_barang]' readonly></td>";
																		for($i=0; $i<$jml_tl; $i++){
																			$ii = $i+1;
																			$qty_tl = get_qty_tl($tl[$i], $isi['id_barang']);
																			echo "<td>
																				<input type='hidden' name='id_tl". $ii ."[$idx]' value='$tl[$i]'>
																				<input type='text' id='qty$i-$idx' name='qty_tl". $ii ."[$idx]' value=". $qty_tl ." onblur='hitung($jml_tl, $idx)'>
																			</td>";
																			$jml += $qty_tl;
																		}
																		echo "<td><input type='text' id='total$idx' name='total[$idx]' value=". $isi['harga_barang'] * $jml ." readonly></td></tr>";
																		$idx++;
																	}
																?>
																</tbody>
																<input type="hidden" name="kondisi" value="<?php echo $kondisi; ?>" />
																<input type="hidden" name="jml_tl" value="<?php echo $jml_tl; ?>" />
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
										<p style="text-align: right"><input type="submit" name="btnSimpan" value="Simpan Setoran" class="btn btn-success"/></p>
									</form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</body>
<?php include "footer.php"; ?>
<script>
	function hitung(i, idx){
		var harga, qty, total;
		harga = document.getElementById("harga"+idx).value;
		
		qty = 0;
		for(j=0; j<i; j++){
			qty += parseInt(document.getElementById("qty"+j+"-"+idx).value);
		}
		total = parseInt(harga) * parseInt(qty);
		document.getElementById("total"+idx).value = total;
	}
</script>
</html>