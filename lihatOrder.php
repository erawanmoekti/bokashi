<?php
	session_start();
	include "koneksi.php";

	$no_order = $_GET['id'];
	$qry = "SELECT pesan.*, kantor.nama_kantor
			  FROM pesan INNER JOIN kantor
			    ON pesan.id_kantor = kantor.id_kantor
			 WHERE pesan.id_pesan = '$no_order'";
	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
	$isi = mysqli_fetch_array($sql);
?>

<?php include "header.php"; ?>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Transaksi Order</title>
</head>

<body>
	<div id="page-wrapper">
		<div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">SALES ORDER</h1>
            </div>
        </div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>No Order</label>
									<label>: <?php echo $no_order; ?></label>
								</div>
								
								<div class="form-group">
									<label>Kantor Unit</label>
									<label>: <?php echo $isi["nama_kantor"]; ?></label>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Tanggal Order</label>
									<label>: <?php echo $isi["tgl_pesan"]; ?></label>
								</div>
								
								<div class="form-group">
									<label>Keterangan</label>
									<label>: <?php echo $isi["keterangan_pesan"]; ?></label>
								</div>
							</div>
						</div>

						<div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-hover" id="dataTables">
								<thead>
									<tr>
										<th>Nama Barang</th>
										<th>Harga</th>
										<th>Qty</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
								<?php
									$i=1;
									$qry = "SELECT detail_pesan.*, barang.nama_barang
											  FROM detail_pesan INNER JOIN barang
												ON detail_pesan.id_barang = barang.id_barang
											 WHERE detail_pesan.id_pesan = '$no_order'
											 GROUP BY detail_pesan.id_barang
												ORDER BY barang.nama_barang";
									$sql = mysqli_query($con, $qry);
									while($isi = mysqli_fetch_array($sql)){
										echo "
											<tr>
												<td>$isi[nama_barang]</td>
												<td>". number_format($isi['harga_satuan_pesan']) . "</td>
												<td>". number_format($isi['jumlah_barang_pesan']) . "</td>
												<td>". number_format($isi['harga_satuan_pesan'] * $isi['jumlah_barang_pesan']) . "</td>
											</tr>
										";
										$i++;
									}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<?php include "footer.php"; ?>
</html>