<?php
	session_start();
	include "koneksi.php";
	if(isset($_POST['btnSimpan'])){
		$i=0;
		$kode = ""; $harga = "";
		foreach($_POST['kode'] as $id){
			$kode = $id; $harga = $_POST['harga'][$i];
			$qry = "UPDATE harga SET harga_barang = '$harga' WHERE id_harga = '$id'";
		
			mysqli_query($con, $qry) or die(mysqli_error($con));
			$i++;
		}
		echo "<script>alert('Harga Barang Berhasil Dirubah')</script>";
	}
	$qry = "SELECT barang.*, harga.*
			  FROM barang INNER JOIN harga
			    ON barang.id_harga = harga.id_harga";
	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
?>

<?php include "header.php"; ?>

<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Master Harga</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-hover" id="dataTables">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Harga</th>
										<th>Harga Barang</th>
									</tr>
								</thead>
								<tbody>
								<?php
			$i=1; $idx_harga = 0;
			while($isi = mysqli_fetch_array($sql)){
				echo "
					<tr>
						<td>$i</td>
						<td>$isi[nama_barang]</td>
						<td>
							<input type='hidden' name='kode[" . $idx_harga . "]' value=$isi[id_harga]>
							<input type='text' name='harga[" . $idx_harga . "]' value=$isi[harga_barang]>
						</td>
					</tr>
					";
					$i++; $idx_harga++;
				}
		?>
								</tbody>
							</table>
							<p style="text-align: right"><input type="submit" name="btnSimpan" value="Simpan" class="btn btn-success"/></p>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		
<?php include "footer.php"; ?>