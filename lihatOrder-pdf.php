<?php
	include "koneksi.php";
	require("libs/fpdf/fpdf.php");
	
	class PDF extends FPDF{
		function Header(){
			global $title;

			$this->SetFont('Arial','B',15);
			$w = $this->GetStringWidth($title)+6;
			$this->SetX((297-$w)/2);
			$this->Cell($w,9,$title,1,1,'C',false);
			$this->Ln(10);
		}

		function Footer(){
			$this->SetY(-15);
			$this->SetFont('Arial','I',8);
			$this->SetTextColor(128);
			$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
		}
		
		function Head(){
			global $no_order, $con;
			
			$qry = "SELECT * FROM pesan WHERE id_pesan = '$no_order'";
			$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
			$isi = mysqli_fetch_array($sql);
			
			$this->SetFont('Arial','',10);
			$this->SetFillColor(200,200,200);
			$this->Cell(30,8,"  No Order",1,0,'',true);
			$this->Cell(50,8,$no_order,1,0,'',false);
			$this->SetX(100);
			$this->Cell(30,8,"  Tgl Order",1,0,'',true);
			$this->Cell(50,8,$isi['tgl_pesan'],1,1,'',false);
			$this->Cell(30,8,"  Keterangan",1,0,'',true);
			$this->Cell(50,8,$isi['keterangan_pesan'],1,0,'',false);
			$this->SetX(100);
			$this->Cell(30,8,"  Total Order",1,0,'',true);
			$this->Cell(50,8,number_format($isi['total_harga_pesan']),1,1,'',false);
			$this->Ln();
		}
		
		function Detail(){
			global $no_order, $con;
			$tl = Array(); $ttl_tl = Array(); $ttl_item; $w;

			$this->SetFillColor(255,0,0);
			$this->SetTextColor(255);
			$this->SetDrawColor(128,0,0);
			$this->SetLineWidth(.3);
			$this->SetFont('Arial','B',10);

			// Header
			$w = 60;
			$this->Cell($w,8,"Nama Barang",'TLB',0,'C',true);
			
			$qry = "SELECT DISTINCT karyawan.* 
					  FROM karyawan INNER JOIN detail_pesan
					    ON karyawan.id_karyawan = detail_pesan.id_karyawan
					 WHERE detail_pesan.id_pesan = '$no_order'
					 ORDER BY karyawan.nama_karyawan";
			$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
			
			while($isi = mysqli_fetch_array($sql)){
				$this->Cell(30,8,$isi['nama_karyawan'],'TB',0,'C',true);
				$tl[] = $isi['id_karyawan'];
				$ttl_tl[] = 0;
				$w += 30;
			}
			$this->Cell(30,8,"Total",'TRB',0,'C',true);
			$w += 30;
			$this->Ln();

			// Color and font restoration
			$this->SetFillColor(224,235,255);
			$this->SetTextColor(0);
			$this->SetFont('Arial','',10);

			// Data
			$fill = false;
			$qry = "SELECT detail_pesan.*, barang.nama_barang, harga.harga_barang
					  FROM detail_pesan INNER JOIN barang
					    ON detail_pesan.id_barang = barang.id_barang INNER JOIN harga
						ON detail_pesan.id_harga = harga.id_harga
					 WHERE detail_pesan.id_pesan = '$no_order'
					 GROUP BY detail_pesan.id_barang
						ORDER BY barang.nama_barang";
			$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
			while($isi = mysqli_fetch_array($sql)){
				$this->Cell(60,8,$isi['nama_barang'],'L',0,'L',$fill);
				$ttl_item = 0; $idx = 0;
				foreach($tl as $item){
					$this->Cell(30,8,$this->GetQty($isi['id_barang'], $item),'',0,'R',$fill);
					$ttl_item += $this->GetQty($isi['id_barang'], $item);
					$ttl_tl[$idx] += $this->GetQty($isi['id_barang'], $item);
					$idx++;
				}
				$this->Cell(30,8,$ttl_item,'R',0,'R',$fill);
				$this->Ln();
				$fill = !$fill;
			}
			// Closing line
			$this->Cell($w,0,'','T');		
			$this->Ln();
			
			//Total 
			$this->SetFillColor(255,0,0);
			$this->SetTextColor(255);
			$this->SetDrawColor(128,0,0);
			$this->SetLineWidth(.3);
			$this->SetFont('Arial','B',10);
			$this->Cell(60,8,"Total Team Leader",'TLB',0,'C',true);
			
			$ttl_item = 0;
			foreach($ttl_tl as $item){
				$this->Cell(30,8,$item,'TB',0,'R',true);
				$ttl_item += $item;
			}
			$this->Cell(30,8,$ttl_item,'TRB',0,'R',true);
		}
		
		function GetQty($id_barang, $id_karyawan){
			global $no_order, $con;

			$qry_qty = "SELECT jumlah_barang_pesan FROM detail_pesan WHERE id_pesan = '$no_order' AND id_barang = '$id_barang' AND id_karyawan = '$id_karyawan'";
			$sql_qty = mysqli_query($con, $qry_qty);
			$isi_qty = mysqli_fetch_array($sql_qty);
			return $isi_qty['jumlah_barang_pesan'];
		}
	}
	
	$pdf = new PDF('L');
	$title = "FAKTUR ORDER BARANG";
	$no_order = $_GET['id'];
	
	$pdf->SetTitle($title);
	$pdf->AddPage();
	$pdf->Head();
	$pdf->Detail();
	//$pdf->FancyTable($header,$data);
	$pdf->Output();
	
	//Cell(width, height, text, border, new line, align, fill)
?>