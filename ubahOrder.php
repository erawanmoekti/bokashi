<?php
	session_start();
	include "koneksi.php";

	$no_order = $_GET['id'];
	$qry = "SELECT pesan.*, kantor.nama_kantor
			  FROM pesan INNER JOIN kantor
			    ON pesan.id_kantor = kantor.id_kantor
			 WHERE pesan.id_pesan = '$no_order'";
	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
	$isi = mysqli_fetch_array($sql);
	
	if(isset($_POST['btnSimpan'])){
		$no_order = $_POST['no_order'];
		$ket_order = $_POST['ket_order'];

		//Update Detail
		$idx = 0; $total = 0;
		foreach($_POST['id_barang'] as $id_barang){
			$qty = $_POST['qty'][$idx];
			$total += $_POST['total'][$idx];

			$qry = "UPDATE detail_pesan SET jumlah_barang_pesan = $qty WHERE id_pesan = '$no_order' AND id_barang = '$id_barang'";
			$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
				
			$idx++;
		}
		
		//Update Head
		$qry = "UPDATE pesan SET total_harga_pesan = $total, keterangan_pesan ='$ket_order' WHERE id_pesan = '$no_order'";
		$sql = mysqli_query($con, $qry) or die(mysqli_error($con));

		header('Location: trOrder.php?ket=sukses_simpan');
	}
?>

<?php include "header.php"; ?>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Transaksi Order</title>
</head>

<body>
	<div id="page-wrapper">
		<div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">SALES ORDER</h1>
            </div>
        </div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<form action="" method="POST">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>No Order</label>
									<label>: <?php echo $no_order; ?></label>
									<input type="hidden" name="no_order" value="<?php echo $no_order; ?>" />
								</div>
								
								<div class="form-group">
									<label>Kantor Unit</label>
									<label>: <?php echo $isi["nama_kantor"]; ?></label>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Tanggal Order</label>
									<label>: <?php echo $isi["tgl_pesan"]; ?></label>
								</div>
								
								<div class="form-group">
									<label>Keterangan</label>
									<input type="text" name="ket_order" value="<?php echo $isi["keterangan_pesan"]; ?>" />
								</div>
							</div>
						</div>

						<div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-hover" id="dataTables">
								<thead>
									<tr>
										<th>Nama Barang</th>
										<th>Harga</th>
										<th>Qty</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
								<?php
									$i=0;
									$qry = "SELECT detail_pesan.*, barang.nama_barang
											  FROM detail_pesan INNER JOIN barang
												ON detail_pesan.id_barang = barang.id_barang
											 WHERE detail_pesan.id_pesan = '$no_order'
											 GROUP BY detail_pesan.id_barang
												ORDER BY barang.nama_barang";
									$sql = mysqli_query($con, $qry);
									while($isi = mysqli_fetch_array($sql)){
										$total = $isi['harga_satuan_pesan'] * $isi['jumlah_barang_pesan'];
										echo "
											<tr>
												<td>$isi[nama_barang]</td>
												<td><input type='text' id='harga$i' value='$isi[harga_satuan_pesan]' readonly></td>
												<td>
													<input type='hidden' name='id_barang[$i]' value='$isi[id_barang]'>
													<input type='text' id='qty$i' name='qty[$i]' value='$isi[jumlah_barang_pesan]' onblur='hitung($i)'>
												</td>
												<td><input type='text' id='total$i' name='total[$i]' value='$total' readonly></td>
											</tr>
										";
										$i++;
									}
								?>
									<tr>
										<td><input type="submit" name="btnSimpan" value="Simpan" /></td>
									</tr>
								</tbody>
							</table>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<?php include "footer.php"; ?>
<script>
	function hitung(idx){
		var harga, qty, total;
		harga = document.getElementById("harga"+idx).value;
		qty = document.getElementById("qty"+idx).value;
		total = parseInt(harga) * parseInt(qty);
		document.getElementById("total"+idx).value = total;
	}
</script
</html>