<?php
	session_start();
	include "koneksi.php";
	$qry = "SELECT * FROM kantor";
	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
?>

<?php include "header.php"; ?>

	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Master Kantor</h1>
				<p style="text-align: right"><a href="formKantor.php"><button type="button" class="btn btn-success">Tambah Data</button></a></p>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-hover" id="dataTables">
								<thead>
									<tr>
										<th>ID Kantor</th>
										<th>Nama Kantor</th>
										<th>Alamat Kantor</th>
										<th>Telepon Kantor</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php
									while($isi = mysqli_fetch_array($sql)){
										echo "
											<tr>
												<td>$isi[id_kantor]</td>
												<td>$isi[nama_kantor]</td>
												<td>$isi[alamat_kantor]</td>
												<td>$isi[telp_kantor]</td>
												<td><a href='hapusKantor.php?id=$isi[id_kantor]'><button type='button' class='btn btn-danger btn-sm'><i class='fa fa-trash'></button></a></td>
											</tr>
										";
									}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		
<?php include "footer.php"; ?>