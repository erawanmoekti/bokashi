<?php
	session_start();
	include "koneksi.php";
	//$qry = "SELECT * FROM karyawan";
	//mysql_query($qry);
	
	if(isset($_POST['username'])){
		$usr = $_POST['username'];
		$pwd = $_POST['password'];
		
		$qry = "SELECT * FROM karyawan WHERE username = '$usr' AND password = '$pwd'";
		$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
		$jml = mysqli_num_rows($sql);
		
		if($jml == 0){
			$ket_error = "Username / Password tidak ditemukan";
		}else{
			$isi = mysqli_fetch_array($sql);
			$_SESSION['id_karyawan'] = $isi['id_karyawan'];
			$_SESSION['akses'] = $isi['status_karyawan'];
			$_SESSION['kantor'] = $isi['id_kantor'];
			header('Location: index.php');
		}
	}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>User Login PT. Karya Pak Oles Tokcer</title>

    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Silahkan Login</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="" method="POST">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="username" type="text" autofocus required>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" required>
                                </div>
								<?php if(isset($ket_error)){ ?>
                                <div class="checkbox">
                                    <label>
                                        <?php echo $ket_error; ?>
                                    </label>
                                </div>
								<?php } ?>
                                <!-- Change this to a button or input when using this as a form -->
                                <input class="btn btn-lg btn-success btn-block" type="submit" value="LOGIN">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <script src="dist/js/sb-admin-2.js"></script>
</body>
</html>