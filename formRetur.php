<?php
	session_start();
	include "koneksi.php";

	$id_kantor = $_SESSION['kantor'];
	$id_karyawan = $_SESSION['id_karyawan'];
	$qry = "SELECT * FROM kantor WHERE id_kantor = '$id_kantor'";
	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
	$isi = mysqli_fetch_array($sql);
	$nama_kantor = $isi["nama_kantor"];
	
	$no_retur = "";
	$tgl_retur = date('Y-m-d');
	$ket_retur = "";
	
	if(isset($_GET['id'])){
		$no_retur = $_GET['id'];
		$kondisi = "Ubah";
		
		$qry = "SELECT * FROM retur WHERE id_retur = '$no_retur'";
		$sql = mysqli_query($con, $qry);
		$isi = mysqli_fetch_array($sql);

		$tgl_retur = $isi['tgl_retur'];
		$ket_retur = $isi['keterangan_retur'];
	}else{
		$qry = "SELECT id_retur FROM retur WHERE MONTH(tgl_retur) = MONTH(CURDATE()) AND YEAR(tgl_retur) = YEAR(CURDATE()) ORDER BY id_retur DESC";
		$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
		$isi = mysqli_fetch_array($sql);
		$tmp = $isi['id_retur'];
		
		if($tmp == ""){
			$no_retur = "RTR-" .date('y') . date('m') . "-0001";
		}else{
			$tmp = substr($tmp, 9, 4) + 1;
			$no_retur = "RTR-" .date('y') . date('m') . "-" . str_pad($tmp, 4, "0", STR_PAD_LEFT);
		}
		
		$kondisi = "Tambah";
	}
	//echo $tmp;
	
	if(isset($_POST['btnSimpan'])){
		$no_retur = $_POST['no_retur'];
		$tgl_retur = $_POST['tgl_retur'];
		$ket_retur = $_POST['ket_retur'];
		$kondisi = $_POST['kondisi'];

		if($kondisi == "Tambah"){
			//Simpan Detail Pesan
			$idx = 0; $total = 0;
			$jml_tl = $_POST['jml_tl'];
			
			foreach($_POST['id_barang'] as $id_barang){
				$id_harga = $_POST['id_harga'][$idx];
				$qry = "SELECT * FROM harga WHERE id_harga = '$id_harga'";
				$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
				$isi = mysqli_fetch_array($sql);
				$harga = $isi['harga_barang'];
				
				for($i=1; $i<=$jml_tl; $i++){
					$tl = $_POST['id_tl'.$i][$idx];
					$qty = $_POST['qty_tl'.$i][$idx];
					$total += $qty * $harga;
					
					$qry = "INSERT INTO detail_retur (id_retur, id_barang, id_harga, id_karyawan, jumlah_barang_retur, harga_satuan_retur)
								 VALUES ('$no_retur', '$id_barang', '$id_harga', '$tl', $qty, $harga)";
					$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
					
					//echo "Barang : $id_barang, Harga : $harga, Qty TL" . $i . " : $qty";
				}
				$idx++;
			}
			
			//Simpan Pesan
			$qry = "INSERT INTO retur (id_retur, tgl_retur, total_harga_retur, keterangan_retur, id_kantor)
						 VALUES ('$no_retur', '$tgl_retur', '$total', '$ket_retur', '$id_kantor')";
			mysqli_query($con, $qry) or die(mysqli_error($con));

			header('Location: trRetur.php?ket=sukses_simpan');
		}else if($kondisi == "Ubah"){
		}
	}
?>

<?php include "header.php"; ?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Retur</title>
</head>

<body>
	<div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Form Retur</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
									<form role="form" action="" method="post">
                                        <div class="form-group">
                                            <label>No Retur</label>
                                            <input class="form-control" type="text" id="no_retur" name="no_retur" value="<?php echo $no_retur; ?>" readonly />
                                        </div>
										
										<div class="form-group">
											<label>Kantor Unit</label>
											<input class="form-control" type="text" name="nama_kantor" value="<?php echo $nama_kantor; ?>" readonly>
										</div>
										
										<div class="form-group">
											<label>Tanggal Retur</label>
                                            <input class="form-control" type="date" name="tgl_retur" value="<?php echo $tgl_retur; ?>" />
										</div>
										
										<div class="form-group">
											<label>Keterangan</label>
                                            <input class="form-control" type="text" name="ket_retur" value="<?php echo $ket_retur; ?>" />
										</div>
										
										<input type="hidden" name="kondisi" value="<?php echo $kondisi; ?>" />
										
										<div class="row">
											<div class="col-lg-12">
												<div class="panel panel-default">
													<div class="panel-body">
														<div class="dataTable_wrapper">
															<table class="table table-striped table-bordered table-hover" id="dataTables">
																<thead>
																	<?php
																	echo "<tr height='30px'><td>Nama Barang</td>";
																	$jml_tl = 0; $tl = Array();
																	$qry = "SELECT * FROM karyawan WHERE id_kantor = '$id_kantor' AND status_karyawan = 'TL'";
																	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
																	while($isi = mysqli_fetch_array($sql)){
																		echo "<td>" . $isi['nama_karyawan'] . "</td>";
																		$jml_tl++; $tl[] = $isi['id_karyawan'];
																	}
																	echo "</tr>";
																	
																	$idx = 0;
																	$qry = "SELECT * FROM barang ORDER BY nama_barang";
																	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
																	while($isi = mysqli_fetch_array($sql)){
																		echo "<tr height='30px'><td>
																			$isi[nama_barang]
																			<input type='hidden' name='id_barang[$idx]' value='$isi[id_barang]'>
																			<input type='hidden' name='id_harga[$idx]' value='$isi[id_harga]'>
																		</td>";
																		for($i=0; $i<$jml_tl; $i++){
																			$ii = $i+1;
																			echo "<td>
																				<input type='hidden' name='id_tl". $ii ."[$idx]' value='$tl[$i]'>
																				<input type='text' name='qty_tl". $ii ."[$idx]' value='0'>
																			</td>";
																		}
																		$idx++;
																	}
																?>
																</thead>
																<tbody>
																
																</tbody>
																<input type="hidden" name="kondisi" value="<?php echo $kondisi; ?>" />
																<input type="hidden" name="jml_tl" value="<?php echo $jml_tl; ?>" />
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
										<p style="text-align: right"><input type="submit" name="btnSimpan" value="Simpan Order" class="btn btn-success"/></p>
									</form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</body>
<?php include "footer.php"; ?>
</html>