-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2016 at 04:51 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bokashidb`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `isi_satuan` int(11) DEFAULT NULL,
  `kadaluarsa` date NOT NULL,
  `jumlah_persediaan` int(11) DEFAULT NULL,
  `id_harga` varchar(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `isi_satuan`, `kadaluarsa`, `jumlah_persediaan`, `id_harga`) VALUES
(10001, 'Minyak Oles Bokashi 35 ml', 0, '2018-05-24', 0, 'HRG10001'),
(10002, 'Minyak Oles Bokashi 10 ml', 0, '2018-05-24', 0, 'HRG10002');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pesan`
--

CREATE TABLE `detail_pesan` (
  `id_pesan` varchar(15) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `id_harga` varchar(9) NOT NULL,
  `jumlah_barang_pesan` int(11) NOT NULL,
  `harga_satuan_pesan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pesan`
--

INSERT INTO `detail_pesan` (`id_pesan`, `id_barang`, `id_harga`, `jumlah_barang_pesan`, `harga_satuan_pesan`) VALUES
('ORD-1606-0001', 10002, 'HRG10002', 11, 20000),
('ORD-1606-0001', 10001, 'HRG10001', 22, 35000);

-- --------------------------------------------------------

--
-- Table structure for table `detail_retur`
--

CREATE TABLE `detail_retur` (
  `id_retur` varchar(15) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `id_harga` int(11) NOT NULL,
  `id_karyawan` int(15) NOT NULL,
  `jumlah_barang_retur` int(11) NOT NULL,
  `harga_satuan_retur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_setoran`
--

CREATE TABLE `detail_setoran` (
  `id_setoran` varchar(15) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `id_harga` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `jumlah_barang_setoran` int(11) NOT NULL,
  `harga_satuan_setoran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_setoran`
--

INSERT INTO `detail_setoran` (`id_setoran`, `id_barang`, `id_harga`, `id_karyawan`, `jumlah_barang_setoran`, `harga_satuan_setoran`) VALUES
('STR-1606-0001', 10002, 0, 222, 1, 20000),
('STR-1606-0001', 10002, 0, 333, 2, 20000),
('STR-1606-0001', 10001, 0, 222, 3, 35000),
('STR-1606-0001', 10001, 0, 333, 4, 35000);

-- --------------------------------------------------------

--
-- Table structure for table `harga`
--

CREATE TABLE `harga` (
  `id_harga` varchar(9) NOT NULL,
  `harga_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga`
--

INSERT INTO `harga` (`id_harga`, `harga_barang`) VALUES
('HRG10001', 35000),
('HRG10002', 20000);

-- --------------------------------------------------------

--
-- Table structure for table `kantor`
--

CREATE TABLE `kantor` (
  `id_kantor` varchar(15) NOT NULL,
  `nama_kantor` varchar(50) NOT NULL,
  `alamat_kantor` varchar(50) NOT NULL,
  `telp_kantor` varchar(50) NOT NULL,
  `status_kantor` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kantor`
--

INSERT INTO `kantor` (`id_kantor`, `nama_kantor`, `alamat_kantor`, `telp_kantor`, `status_kantor`) VALUES
('CBG0001	', 'IKOT Surabaya', 'Jl. Raya Waru Komplek Ruko Gateway B-24, SIdoarjo', '031-8554566', ''),
('UNT001', 'Unit Surabaya', 'Jl. Barata Jaya XX No. 25 Surabaya', '031-5046766', ''),
('UNT002', 'Unit Sidoarjo', 'Perum Sidokare Asri Blok BA-01, Sepande, Kec. Cand', '031-8968534', ''),
('UNT003', 'Unit Malang', 'Jl. Satsui Tubun 25 C, Kel. Kebonsari, Kec. Sukun ', '0341-833488', ''),
('UNT004', 'Unit Lamongan', 'Perum Deket Permai, Jl. Nangka No. 05, Kel Deket -', '0322-315092', ''),
('UNT005', 'Pos Gresik', 'Jl. KH. Syafi i No. 01 Bunder, Gresik', '031-77622969', ''),
('UNT006', 'Unit Madiun', 'Perumnas I, Manisrejo, Jl. Ardimanis No. F4/29, Ko', '0351-458590', '');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id_karyawan` int(11) NOT NULL,
  `nama_karyawan` varchar(50) NOT NULL,
  `jabatan_karyawan` varchar(50) NOT NULL,
  `status_karyawan` varchar(50) NOT NULL,
  `id_kantor` varchar(15) NOT NULL,
  `username` varchar(15) DEFAULT NULL,
  `password` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `nama_karyawan`, `jabatan_karyawan`, `status_karyawan`, `id_kantor`, `username`, `password`) VALUES
(111, 'Coba 1', 'Admin Unit', 'Unit', 'UNT001', 'coba', 'coba'),
(222, 'Team Leader A', 'Team Leader', 'TL', 'UNT001', '', ''),
(333, 'Team Leader B', 'Team Leader', 'TL', 'UNT001', '', ''),
(444, 'Team Leader C', 'Team Leader', 'TL', 'UNT002', '', ''),
(1332510081, 'Gardito Lukman', 'Entry Data', 'Cabang', 'CBG0001', 'Bokashi', 'Bokashi');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id_pesan` varchar(15) NOT NULL,
  `tgl_pesan` date NOT NULL,
  `total_harga_pesan` int(11) NOT NULL,
  `keterangan_pesan` varchar(50) NOT NULL,
  `id_kantor` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id_pesan`, `tgl_pesan`, `total_harga_pesan`, `keterangan_pesan`, `id_kantor`) VALUES
('ORD-1606-0001', '2016-06-13', 990000, 'coba keterangan', 'UNT001');

-- --------------------------------------------------------

--
-- Table structure for table `retur`
--

CREATE TABLE `retur` (
  `id_retur` varchar(15) NOT NULL,
  `tgl_retur` date NOT NULL,
  `total_harga_retur` int(11) NOT NULL,
  `keterangan_retur` varchar(50) NOT NULL,
  `id_kantor` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `setoran`
--

CREATE TABLE `setoran` (
  `id_setoran` varchar(15) NOT NULL,
  `tgl_setoran` date NOT NULL,
  `total_harga_setoran` int(11) NOT NULL,
  `keterangan_setoran` varchar(50) NOT NULL,
  `id_kantor` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setoran`
--

INSERT INTO `setoran` (`id_setoran`, `tgl_setoran`, `total_harga_setoran`, `keterangan_setoran`, `id_kantor`) VALUES
('STR-1606-0001', '2016-06-14', 305000, '', 'UNT001');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `harga`
--
ALTER TABLE `harga`
  ADD PRIMARY KEY (`id_harga`);

--
-- Indexes for table `kantor`
--
ALTER TABLE `kantor`
  ADD PRIMARY KEY (`id_kantor`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id_pesan`);

--
-- Indexes for table `retur`
--
ALTER TABLE `retur`
  ADD PRIMARY KEY (`id_retur`);

--
-- Indexes for table `setoran`
--
ALTER TABLE `setoran`
  ADD PRIMARY KEY (`id_setoran`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
