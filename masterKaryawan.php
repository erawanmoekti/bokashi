<?php
	session_start();
	include "koneksi.php";
	$qry = "SELECT karyawan.*, kantor.nama_kantor FROM karyawan INNER JOIN kantor ON karyawan.id_kantor = kantor.id_kantor";
	$sql = mysqli_query($con, $qry) or die(mysqli_error($con));
?>

<?php include "header.php"; ?>

<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Master Karyawan</h1>
				<p style="text-align: right"><a href="formKaryawan.php"><button type="button" class="btn btn-success">Tambah Data</button></a></p>
			</div>
		</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover" id="dataTables">
									<thead>
										<tr>
											<th>ID Karyawan</th>
											<th>Nama Karyawan</th>
											<th>Jabatan Karyawan</th>
											<th>Status Karyawan</th>
											<th>Kantor</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
									<?php
										while($isi = mysqli_fetch_array($sql)){
											echo "
												<tr>
													<td>$isi[id_karyawan]</td>
													<td>$isi[nama_karyawan]</td>
													<td>$isi[jabatan_karyawan]</td>
													<td>$isi[status_karyawan]</td>
													<td>$isi[nama_kantor]</td>
													<td><a href='hapusKaryawan.php?id=$isi[id_karyawan]'><button type='button' class='btn btn-danger btn-sm'><i class='fa fa-trash'></button></a></td>
												</tr>
											";
										}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>

<?php include "footer.php"; ?>